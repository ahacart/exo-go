package main

import (
	"flag"
	"fmt"
	"os"

	"framagit.org/gauthier/exo-go/comptermots"
)

var printHelp bool
var phrase string

func setFlags(printHelp *bool) {
	flag.BoolVar(printHelp, "help", false, "Afficher ce message d'aide.")
	flag.StringVar(&phrase, "i", "", "phrase")
}

func main() {
	setFlags(&printHelp)
	flag.Parse()
	if printHelp {
		fmt.Println("-----------------------------")
		fmt.Println("Usage:")
		flag.PrintDefaults()
		fmt.Println("-----------------------------")
		os.Exit(0)
	}

	nbmot := comptermots.Count(phrase)
	fmt.Println(nbmot)
}
